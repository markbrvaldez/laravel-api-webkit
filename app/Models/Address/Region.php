<?php

namespace App\Models\Address;

use App\QueryFilters\Address\CodeFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Pipeline\Pipeline;

/**
 * App\Models\Address\Region
 *
 * @property int $id
 * @property string $code_correspondence
 * @property string $code
 * @property string $name
 * @property string $alt_name
 * @property string $geo_level
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static Builder|Region filtered()
 * @method static Builder|Region newModelQuery()
 * @method static Builder|Region newQuery()
 * @method static Builder|Region query()
 * @method static Builder|Region whereAltName($value)
 * @method static Builder|Region whereCode($value)
 * @method static Builder|Region whereCodeCorrespondence($value)
 * @method static Builder|Region whereCreatedAt($value)
 * @method static Builder|Region whereGeoLevel($value)
 * @method static Builder|Region whereId($value)
 * @method static Builder|Region whereName($value)
 * @method static Builder|Region whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Region extends Model
{
    use HasFactory;

    /**
     * The properties that are mass-assignable
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'code',
        'name',
        'alt_name',
        'geo_level',
        'code_correspondence',
    ];

    /**
     * @Scope
     * Pipeline for HTTP query filters
     */
    public function scopeFiltered(Builder $builder): Builder
    {
        return app(Pipeline::class)
            ->send($builder)
            ->through([
                CodeFilter::class,
            ])
            ->thenReturn();
    }

    /**
     * A region comprises an address
     *
     * @returns HasMany
     */
    protected function address(): HasMany
    {
        return $this->hasMany(Address::class);
    }
}
