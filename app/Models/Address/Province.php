<?php

namespace App\Models\Address;

use App\QueryFilters\Address\CodeFilter;
use App\QueryFilters\Address\RegionFilter as RegionFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Pipeline\Pipeline;

/**
 * App\Models\Address\Province
 *
 * @property int $id
 * @property string $code_correspondence
 * @property int $region_id
 * @property string $code
 * @property string $name
 * @property string $geo_level
 * @property string|null $old_name
 * @property string|null $income_classification
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static Builder|Province filtered()
 * @method static Builder|Province newModelQuery()
 * @method static Builder|Province newQuery()
 * @method static Builder|Province query()
 * @method static Builder|Province whereCode($value)
 * @method static Builder|Province whereCodeCorrespondence($value)
 * @method static Builder|Province whereCreatedAt($value)
 * @method static Builder|Province whereGeoLevel($value)
 * @method static Builder|Province whereId($value)
 * @method static Builder|Province whereIncomeClassification($value)
 * @method static Builder|Province whereName($value)
 * @method static Builder|Province whereOldName($value)
 * @method static Builder|Province whereRegionId($value)
 * @method static Builder|Province whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Province extends Model
{
    use HasFactory;

    /**
     * The properties that are mass-assignable
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'code',
        'name',
        'region_id',
        'code_correspondence',
        'geo_level',
        'old_name',
        'income_classification',
    ];

    /**
     * @Scope
     * Pipeline for HTTP query filter
     */
    public function scopeFiltered(Builder $builder): Builder
    {
        return app(Pipeline::class)
            ->send($builder)
            ->through([
                CodeFilter::class,
                RegionFilter::class,
            ])
            ->thenReturn();
    }

    /**
     * A province comprises an address
     *
     * @returns HasMany
     */
    protected function address(): HasMany
    {
        return $this->hasMany(Address::class);
    }

    /**
     * A province belongs to region
     *
     * @returns BelongsTo
     */
    protected function region(): BelongsTo
    {
        return $this->belongsTo(Region::class);
    }
}
