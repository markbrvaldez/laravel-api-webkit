<?php

namespace App\Models;

use App\Enums\AppTheme;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppSettings
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $theme
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AppSettings whereValue($value)
 *
 * @mixin \Eloquent
 */
class AppSettings extends Model
{
    use HasFactory;

    protected $table = 'app_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'value',
    ];

    /**
     * The attributes that should be casts
     *
     * @var array
     */
    protected $casts = [
        'theme' => AppTheme::class,
    ];
}
