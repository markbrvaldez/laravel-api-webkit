<?php

namespace App\Enums;

/*
|--------------------------------------------------------------------------
| Api error Code
|--------------------------------------------------------------------------
|
| Provides a list of error codes we can send back to the client
|
*/

enum ApiErrorCode: string
{
    case VALIDATION = 'VALIDATION_ERROR_CODE';
    case RESOURCE_NOT_FOUND = 'RESOURCE_NOT_FOUND_ERROR_CODE';
    case INVALID_CREDENTIALS = 'INVALID_CREDENTIALS_ERROR_CODE';
    case SMTP_ERROR_CODE = 'SMTP_ERROR_CODE';
    case UNAUTHORIZED = 'UNAUTHORIZED_ERROR_CODE';
    case FORBIDDEN = 'FORBIDDEN_ERROR_CODE';
    case UNKNOWN_ROUTE = 'UNKNOWN_ROUTE_ERROR_CODE';
    case RATE_LIMIT = 'TOO_MANY_REQUESTS_ERROR_CODE';
    case DEPENDENCY_ERROR_CODE = 'DEPENDENCY_ERROR_CODE';
    case SERVER = 'SERVER_ERROR_CODE';
    case INCORRECT_OLD_PASSWORD = 'INCORRECT_OLD_PASSWORD_ERROR_CODE';
    case PAYLOAD_TOO_LARGE = 'PAYLOAD_TOO_LARGE_ERROR_CODE';
    case EMAIL_NOT_VERIFIED = 'EMAIL_NOT_VERIFIED_ERROR_CODE';
    case BAD_REQUEST = 'BAD_REQUEST_ERROR_CODE';
}
