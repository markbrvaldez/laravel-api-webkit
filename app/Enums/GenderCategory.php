<?php

namespace App\Enums;

enum GenderCategory: string
{
    case MALE = '1';
    case FEMALE = '2';
}
