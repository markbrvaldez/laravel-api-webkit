<?php

namespace App\Enums;

enum MunicipalClassification: string
{
    case CITY = '1';
    case MUNICIPALITY = '2';
}
