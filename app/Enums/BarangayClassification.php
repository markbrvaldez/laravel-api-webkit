<?php

namespace App\Enums;

enum BarangayClassification: string
{
    case URBAN = '1';
    case RURAL = '2';
}
