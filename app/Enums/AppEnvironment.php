<?php

namespace App\Enums;

enum AppEnvironment: string
{
    case LOCAL = 'local';
    case DEVELOPMENT = 'development';
    case UAT = 'uat';
    case PRODUCTION = 'production';
}
