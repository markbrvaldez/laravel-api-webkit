<?php

namespace App\Enums;

enum Role: string
{
    case ADMIN = 'admin';
    case STANDARD_USER = 'standard_user';
    case SYSTEM_SUPPORT = 'system_support';
    case SUPER_USER = 'super_user';
}
