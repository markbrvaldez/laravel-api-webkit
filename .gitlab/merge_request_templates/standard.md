## 🚀 Deployment Guide
- Run `php artisan db:seed --class=SeederMe`
- Add these missing `env` variables
- Attach a PDF doc if needed


## 🗃️ Migrations
- 2024_04_01_12340_create_table_a.php
- 2024_04_02_010719_alter_column_a_table_name.php => `table_b` table


## 📦 Packages
- Installed package A: https://link-to-package.com

## Run Code Formatter
php artisan app:styler